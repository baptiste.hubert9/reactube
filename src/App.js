import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import Channel from "./components/Channel";
import DetailedVideo from "./components/DetailedVideo";
import DetailedChannel from "./components/DetailedChannel";
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
    const [videos, setVideos] = useState([]);
    const [channels, setChannels] = useState([]);
    const [selectedVideo, selectVideo] = useState({});
    const [selectedChannel, selectChannel] = useState({});
    const [type, changeType] = useState('video');

    function set(){
        if (type == 'video'){
            return setVideos;
        } else {
            return setChannels;
        }
    }
    return (
        <Container className="p-3">
            <Router>
                <MyNav onResults={set()} changeType={changeType} type={type}/>

                <Switch>
                    <Route path="/videos/:videoId">
                        <DetailedVideo id={selectedVideo.id}
                                       snippet={selectedVideo.snippet} player={selectedVideo.player}
                                       statistics={selectedVideo.statistics} />
                    </Route>
                    <Route path="/channels/:channelId">
                        <DetailedChannel id={selectedChannel.id}
                                       snippet={selectedChannel.snippet} player={selectedChannel.player}
                                       statistics={selectedChannel.statistics} />
                    </Route>
                    <Route path="/search/channel/:search">
                            <>
                                {channels.map(c => {
                                    const {
                                        thumbnails, channelTitle, description, publishedAt
                                    } = c.snippet;
    
                                    return (<Channel
                                        channelId={c.id.channelId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishAt={publishedAt}
                                        selectChannel={selectChannel}/>);
                                })}
                        </>
                    </Route>
                    <Route path="/search/video/:search">
                            <>
                                {videos.map(v => {
                                    const {
                                        title, description, thumbnails,
                                        channelTitle, publishTime
                                    } = v.snippet;
    
                                    return (<Video
                                        videoId={v.id.videoId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishTime={publishTime}
                                        title={title}
                                        selectVideo={selectVideo}/>);
                                })}
                        </>
                    </Route>
                    <Route path="/">
                        Merci d'effectuer une recherche...
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
