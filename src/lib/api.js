import axios from 'axios'

const key = 'AIzaSyDP7OP2uTN-9NgCJIgIKEbZfXtxlZxcuKw'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})

export default fetcher;