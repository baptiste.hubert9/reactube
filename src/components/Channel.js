import React from 'react';
import Card from 'react-bootstrap/Card';
import {Col, Image, Row} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";

const Channel = ({channelId, thumbnail, description, channelTitle, publishedAt, selectChannel}) => {
    const history = useHistory();
    const search = async () => {
        console.log("test");
        const resp = await api.get('/channels', {
            params: {
                id: channelId,
                part: 'snippet',
            }
        });
        console.log('Received', resp.data.items);
        selectChannel(resp.data.items[0]);
        history.push(`/channels/${channelId}`);
    };

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Row>
                <Col xs={3}>
                    <Image src={thumbnail.url}
                           fluid={true}
                           rounded
                           onClick={search}/>
                </Col>
                <Col>
                    <Card.Title onClick={search}>{channelTitle}</Card.Title>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text>{publishedAt}</Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>)
};

export default Channel;