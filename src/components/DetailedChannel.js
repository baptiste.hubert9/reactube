import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import {useHistory} from "react-router-dom";

const DetailedChannel = ({id, snippet}) => {
    const {channelTitle, description, publishedAt} = snippet;
    const history = useHistory();

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Button variant="danger" style={{float: 'right'}} onClick={history.goBack}>Retour</Button>
            <Row>
                <Col>
                    <h2>{channelTitle}</h2>
                    <Card.Text dangerouslySetInnerHTML={{__html: description.replace(/\n/g, '<br/>')}}/>
                    <Card.Text className="mb-1 text-muted">{publishedAt}</Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>);
}

export default DetailedChannel;