import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from './SearchBar';

const MyNav = ({onResults, changeType, type}) => {
    return (<Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Reactube</Navbar.Brand>
        <Nav className="mr-auto">
            <Nav.Link onClick={() => changeType("video")}>Videos</Nav.Link>
            <Nav.Link onClick={() => changeType("channel")}>Channels</Nav.Link>
        </Nav>
        <SearchBar onResults={onResults} type={type} />
    </Navbar>);
}

export default MyNav;